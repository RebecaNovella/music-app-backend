drop table if exists usuario;
create table usuario (
    id integer primary key autoincrement, 
    nombre text not null,
    telefono text not null, 
    correo text not null, 
    clave text not null, 
    rol text not null, 
    turno text 
);

drop table if exists producto;
create table producto (
    id integer primary key autoincrement, 
    url_portada text not null,
    titulo text not null,
    artista text not null,
    precio real not null,
    valoracion real not null,
    categoria text not null
);

drop table if exists review;
create table review (
    producto integer not null,
    autor text not null,
    valoracion real not null,
    opinion text not null,
    foreign key (producto) references producto (id)
);

drop table if exists direccion;
create table direccion (
    usuario integer not null,
    nombre text not null,
    pais text not null,
    cp text not null,
    ciudad text not null,
    direccion text not null,
    tipo text not null,
    foreign key (usuario) references usuario (id)
);

drop table if exists pago;
create table pago (
    usuario integer not null,
    nombre text not null,
    num_tarjeta text not null,
    fecha_exp text not null,
    cvv text not null,
    foreign key (usuario) references usuario (id)
);

drop table if exists venta;
create table venta (
    monto real not null,
    fecha_hora text not null
);

drop table if exists descuento;
create table descuento (
    porcentaje real not null,
    fecha_inicio text not null, 
    fecha_final text not null
);

