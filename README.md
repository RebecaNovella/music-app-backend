Crear un ambiente de python
`python -m venv venv`

instalar sqlite

Activar el ambiente de python
`source venv/bin/activate`

instalar dependencias
`pip -r requirements.txt`

Correr el backend desde el directorio src
`python app.py`