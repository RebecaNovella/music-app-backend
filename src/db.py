import sqlite3
def get_connection():
    conn = sqlite3.connect('database.db')
    conn.row_factory = dict_factory
    return conn

def dict_factory(cursor, row):
    fields = [column[0] for column in cursor.description]
    return {key: value for key, value in zip(fields, row)}

def execute_query(query, params):
    conn = get_connection()
    conn.execute(query, params)
    conn.commit()
    conn.close()
