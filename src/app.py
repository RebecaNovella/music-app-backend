from flask import Flask, request
from db import execute_query, get_connection
from flask_cors import CORS

app = Flask(__name__)
cors = CORS(app)
app.config['DEBUG'] = True

####### Endpoints Usuario
@app.route('/usuarios')
def get_usuarios() :
    return list_table('usuario')

@app.route('/usuarios', methods=['POST'])
def save_usuario():
    return master_insert('usuario', ['nombre', 'telefono', 'correo', 'clave', 'rol', 'turno'])


@app.route('/usuarios', methods=['PUT'])
def update_usuario() :
    return json_execute('''update usuario set nombre = ?, telefono = ?, correo = ?, clave = ?, turno = ? where id = ?''', ('nombre', 'telefono', 'correo', 'clave', 'turno', 'id'))


@app.route('/usuarios', methods=['DELETE'])
def delete_usuario() :
    return json_execute('''delete from usuario where id = ?''', ('id',))


####### Endpoints producto

@app.route('/productos')
def get_productos() :
    return list_table('producto')

@app.route('/productos', methods=['POST'])
def save_producto():
    return master_insert('producto', ['url_portada', 'titulo', 'artista', 'precio', 'valoracion', 'categoria'])

@app.route('/productos', methods=['PUT'])
def update_producto():
    return json_execute(''' 
    update producto set url_portada=?, titulo=?, artista=?, precio=?, valoracion=?, categoria=?
    where id = ?''', 
    ('url_portada', 'titulo', 'artista', 'precio', 'valoracion', 'categoria', 'id'))

@app.route('/productos', methods=['DELETE'])
def delete_producto():
    return json_execute(''' 
    delete from producto  where id = ? ''', 
    ('id',))

###### Endpoints venta
@app.route('/ventas')
def get_ventas() :
    return list_table('venta')

@app.route('/ventas', methods=['POST'])
def save_venta():
    return master_insert('venta', ['monto', 'fecha_hora'])

####### Endpoint decuento
@app.route('/descuentos')
def get_descuento() :
    return list_table('descuento')

@app.route('/descuentos', methods=['POST'])
def save_descuento():
    return master_insert('descuento', ['porcentaje', 'fecha_inicio', 'fecha_final'])

####### Enpoint direccion
@app.route('/direcciones/<id_usuario>')
def get_direcciones(id_usuario):
    direcciones = list_table('direccion')
    return [direccion for direccion in direcciones if direccion['usuario'] == int(id_usuario)]

@app.route('/direcciones', methods=['POST'])
def save_direcciones():
    return master_insert('direccion', ['usuario', 'nombre', 'pais', 'cp', 'ciudad', 'direccion', 'tipo'])

########## Endpoint pago
@app.route('/pagos/<id_usuario>')
def get_pagos(id_usuario):
    pagos = list_table('pago')
    return [pago for pago in pagos if pago['usuario'] == int(id_usuario)]

@app.route('/pagos', methods=['POST'])
def save_pagos():
    return master_insert('pago', ['usuario', 'nombre', 'num_tarjeta', 'fecha_exp', 'cvv'])

##### Endpoint review 
@app.route('/reviews/<id_producto>')
def get_reviews(id_producto):
    reviews = list_table('review')
    return [review for review in reviews if review['producto'] == int(id_producto)]

@app.route('/reviews', methods=['POST'])
def save_review():
    return master_insert('review', ['producto', 'autor', 'valoracion', 'opinion'])


#### Endpoints login
@app.route('/login', methods=['POST'])
def login():
    credenciales = request.json
    usuario = get_user_by_email(credenciales['correo'])
    password = credenciales['clave']
    if not usuario or usuario['clave'] != password :
        return {'valid':False}
    return {'valid':True, 'usuario':usuario}  



def get_user_by_email(email):
  usuarios = list_table('usuario')
  filtered = [u for u in usuarios if u["correo"] == email]
  if (len(filtered) == 0):
    return None
  else:
    return filtered[0]


def list_table(table_name) :
    conn = get_connection()
    consulta = 'select * from ' + table_name
    items = conn.execute(consulta).fetchall()
    conn.close()
    return items

def master_insert(table_name, col_names):
    values = ", ".join(['?']*len(col_names))
    consulta = f'insert into {table_name} ({", ".join( col_names)}) values({values})'
    return json_execute(consulta, col_names)  

def json_execute(query, params):
    data = request.json
    execute_query(query, [data[p] for p in params])
    return data


app.run()